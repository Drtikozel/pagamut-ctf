/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package roles;

import ctfbot.CTFMaster;
import java.util.List;
import java.util.Stack;

/**
 * roles of bots - there is only one defender and one leader, others are support
 * (followers of leader)
 *
 * @author root
 */
final class RoleAssignment {

    private static volatile Stack<Role> availableRoles = new Stack<Role>();

    static {
        availableRoles.push(Role.LEADER);
        availableRoles.push(Role.DEFENDER);
    }
    
    /**
     * Can't instantiate
     */
    private RoleAssignment() {}
    
    public static synchronized void relinquishRole(Role botRole) {
        if (botRole != Role.SUPPORT) {
            availableRoles.push(botRole);
        }
    }
    
    /**
     * Assign available role to bot
     * @return
     */
    public static synchronized Role getAvailableRole() {
        return (availableRoles.isEmpty()) ? Role.SUPPORT : availableRoles.pop();
    }

}
