/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package roles;

import action.Navigate;
import ctfbot.CTFMaster;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author root
 */
public class RoleService {

    private final CTFMaster bot;
    private volatile static List<CTFMaster> bots = new ArrayList<CTFMaster>();

    private Role botRole;

    public RoleService(CTFMaster bot) {
        this.bot = bot;
        bots.add(bot);
    }

    public void reset() {
        relinquishRole();
        getRole();
    }

    protected void relinquishRole() {
        Role role = botRole;

        // nothing to pass
        if (role == null) {
            return;
        } else {
            botRole = null;
            boolean isTaken = false;

            // notify other bots about available role
            for (CTFMaster bot : bots) {
                if (this.bot.equals(bot)) {
                    continue;
                }

                if (bot.getRoleService().roleTakeOver(role, this.bot)) {
                    isTaken = true;
                    break;
                }
            }

            if (!isTaken) { // no one took the role
                RoleAssignment.relinquishRole(role); // put it back to stack
            }
        }
    }

    /**
     * Assign available role to bot
     *
     * @return
     */
    public Role getRole() {
        if (botRole == null) {
            botRole = RoleAssignment.getAvailableRole();
        }

        return botRole;
    }

    /**
     * Checks if the role is sutable to take over and then accept or deny
     *
     * @param role role that other bot offered for some purpose
     * @return if the suggested role was taken
     */
    protected boolean roleTakeOver(Role passedRole, CTFMaster otherBot) {

        boolean wantsRole = false;

        if (botRole == null) {
            wantsRole = true;
            bot.getLog().info("$$ Take over role " + passedRole.name());
        } else {
            switch (botRole) {
                case DEFENDER:
                    wantsRole = doesDefenderWantsRole(passedRole, otherBot);
                    break;
                case LEADER:
                    wantsRole = false;
                    break;
                case SUPPORT:
                    wantsRole = doesSupportWantsRole(passedRole, otherBot);
                    break;
            }

            if (wantsRole) {
                bot.getLog().info("$$ Changing role from " + botRole.name() + " to " + passedRole.name());
//            RoleAssignment.relinquishRole(botRole);
                this.relinquishRole();
            }
        }

        if (wantsRole) {
            botRole = passedRole;
            bot.changeNameByRole();
        }

        return wantsRole;
    }

    private boolean doesDefenderWantsRole(Role passedRole, CTFMaster otherBot) {
        switch (passedRole) {
            case SUPPORT:
                double distanceToPlayer = bot.getFwMap().getDistance(bot.getInfo().getNearestNavPoint(), otherBot.getInfo().getNearestNavPoint());
//                double playerDistanceToOtherBase = bot.getFwMap().getDistance(bot.getInfo().getNearestNavPoint(), otherBot.getInfo().getNearestNavPoint());
                return distanceToPlayer < Navigate.getHalfMapDistance();
            default:
                return false;
        }
    }

    private boolean doesSupportWantsRole(Role passedRole, CTFMaster otherBot) {

        switch (passedRole) {
            case LEADER:
                return true;
//            case DEFENDER:
//                // Only if bot is max. about 17% on path to enemy base
//                return bot.getInfo().getDistance(bot.getCTF().getOurBase()) < (Navigate.getHalfMapDistance() / 3);
            default:
                return false;
        }
    }

}
