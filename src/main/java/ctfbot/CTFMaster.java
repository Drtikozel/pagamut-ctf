package ctfbot;

import action.AAction;
import roles.RoleService;
import action.Navigate;
import action.Fight;
import action.FlagCapture;
import cz.cuni.amis.pogamut.base.agent.impl.AgentId;
import cz.cuni.amis.pogamut.base.communication.worldview.listener.annotation.EventListener;
import cz.cuni.amis.pogamut.base.communication.worldview.listener.annotation.ObjectClassListener;
import cz.cuni.amis.pogamut.base.communication.worldview.object.IWorldObjectEvent;
import cz.cuni.amis.pogamut.base.utils.guice.AgentScoped;
import cz.cuni.amis.pogamut.unreal.communication.messages.UnrealId;
import cz.cuni.amis.pogamut.ut2004.agent.module.utils.TabooSet;
import cz.cuni.amis.pogamut.ut2004.agent.navigation.IUT2004Navigation;
import cz.cuni.amis.pogamut.ut2004.bot.impl.UT2004Bot;
import cz.cuni.amis.pogamut.ut2004.bot.params.UT2004BotParameters;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbcommands.Initialize;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbcommands.SendMessage;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.BotDamaged;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.BotKilled;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.ConfigChange;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.FlagInfo;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.GameInfo;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.InitedMessage;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.NavPoint;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.Player;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.Self;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.Spawn;
import cz.cuni.amis.pogamut.ut2004.teamcomm.bot.UT2004BotTCController;
import cz.cuni.amis.pogamut.ut2004.teamcomm.bot.UT2004TCClient;
import cz.cuni.amis.pogamut.ut2004.teamcomm.server.UT2004TCServer;
import cz.cuni.amis.pogamut.ut2004.utils.UT2004BotRunner;
import cz.cuni.amis.utils.exception.PogamutException;
import helper.CTFMasterParameters;
import helper.DefenderPost;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * Pogamut's "Hello world!" example showing few extra things such as
 * introspection and various bot-initializing methods.
 * <p>
 * <p>
 * First, try to run the bot and kill it... than uncomment the line inside
 * {@link EmptyBot#botKilled(BotKilled)} and run the bot again kill it to see
 * the difference.
 *
 * @author Michal Bida aka Knight
 * @author Rudolf Kadlec aka ik
 * @author Jakub Gemrot aka Jimmy
 */
@AgentScoped
public class CTFMaster extends UT2004BotTCController {

    private static volatile int agentOrder = 0;

    private TabooSet<NavPoint> tabooNavPoints;

    private IUT2004Navigation nav;
    protected UT2004TCClient com = this.getTCClient();
    protected Navigate col;
    protected FlagCapture flagCapture;
    protected Fight fight;
//    protected KnowledgeBase ac;

    public final int LOW_HEALTH = 40;
    private RoleService roleService;
    private String customName;
    private Navigate smartNav;

    private List<AAction> actions;
    private static UT2004TCServer TCServer;
    private BotState state;

    /**
     * Initialize all necessary variables here, before the bot actually receives
     * anything from the environment.
     */
    @Override
    public void prepareBot(UT2004Bot bot) {
//        bot.getLogger().setLevel(Level.ALL);
//        bot.getParams()
    }

    public Integer getTeam() {
        return bot.getParams().getTeam();
    }

    private CTFMasterParameters getBotParams() {
        return (CTFMasterParameters) bot.getParams();
    }

//    @ObjectClassListener(objectClass = FlagInfo.class)
//    public void flagStateChanged(IWorldObjectEvent event) {
//        FlagInfo flag = (FlagInfo) event.getObject();
//
//    }
    @Override
    public IUT2004Navigation getNavigation() {
        if (this.nav == null) {
            if (nmNav.isAvailable()) {
                log.info("Navigation: Using NavMesh");
                this.nav = nmNav;
            } else {
                log.info("Navigation: NavMesh not available, fallback to FW NavPoint navigation");
                this.nav = navigation;
            }
        }

        return this.nav;
    }

    public FlagCapture getFlagCaptureModule() {
        return flagCapture;
    }

    public Fight getFightModule() {
        return fight;
    }

    /**
     * Here we can modify initializing command for our bot, e.g., sets its name
     * or skin.
     *
     * @return instance of {@link Initialize}
     */
    @Override
    public Initialize getInitializeCommand() {
        final Initialize init = new Initialize();
        init.setDesiredSkill(getBotParams().getSkill());

        roleService = new RoleService(this);
        this.customName = "Cigi" + getOrder();
        return init;
    }
    
    public void changeNameByRole() {
        config.setName(this.customName + " (" + roleService.getRole() + ")");
    }
    
    private synchronized int getOrder() {
        return ++agentOrder;
    }

    public String getCustomName() {
        return customName;
    }
    
    @Override
    public void mapInfoObtained() {
        // YOU CAN USE navBuilder IN HERE
        // IN WHICH CASE YOU SHOULD UNCOMMENT FOLLOWING LINE AFTER EVERY CHANGE
//        navMeshModule.setReloadNavMesh(true); // tells NavMesh to reconstruct OffMeshPoints    	
    }

    /**
     * Handshake with GameBots2004 is over - bot has information about the map
     * in its world view. Many agent modules are usable since this method is
     * called.
     *
     * @param gameInfo informaton about the game type
     * @param config information about configuration
     * @param init information about configuration
     */
    @Override
    public void botInitialized(GameInfo gameInfo, ConfigChange currentConfig, InitedMessage init) {
        tabooNavPoints = new TabooSet<NavPoint>(bot);
        items.setPathPlanner(fwMap);
        this.state = BotState.GETTING_READY;
//        this.ac = new KnowledgeBase(this);
    }

    /**
     * The bot is initilized in the environment - a physical representation of
     * the bot is present in the game.
     *
     * @param gameInfo informaton about the game type
     * @param config information about configuration
     * @param init information about configuration
     * @param self information about the agent
     */
    @Override
    public void botFirstSpawn(GameInfo gameInfo, ConfigChange config, InitedMessage init, Self self) {
        this.smartNav = new Navigate(this);
        this.fight = new Fight(this);
        this.flagCapture = new FlagCapture(this);

        actions = Arrays.asList(
                this.fight,
                this.flagCapture,
                this.smartNav
        );
        
        // alternatively, you may use getAct() method for issuing arbitrary {@link CommandMessage} for the bot's body
        // inside UT2004
//        getAct().act(new SendMessage().setGlobal(true).setText("And I can speak! Hurray!"));
    }

    public Navigate getNavigationModule() {
        return smartNav;
    }
    
    public long getSimTime() {
        return getBot().getSelf().getSimTime();
    }
    
    /**
     * Called each time the bot dies. Good for reseting all bot's state
     * dependent variables.
     *
     * @param event
     */
    @Override
    public void botKilled(BotKilled event) {
        for (AAction action : actions) {
            action.reset();
        }
        roleService.reset();
        this.state = BotState.GETTING_READY;
    }

    public BotState getState() {
        return state;
    }
    
    /**
     *
     * @param event bot spawned event
     */
    @EventListener(eventClass = Spawn.class)
    public void botSpawned(Spawn event) {
        changeNameByRole();
    }

//    public KnowledgeBase getAbilities() {
//        return ac;
//    }
    @Override
    public void beforeFirstLogic() {
//        navMeshModule.getNavMeshDraw().draw(true, true);
    }

    /**
     * Main method that controls the bot - makes decisions what to do next. It
     * is called iteratively by Pogamut engine every time a synchronous batch
     * from the environment is received. This is usually 4 times per second - it
     * is affected by visionTime variable, that can be adjusted in GameBots ini
     * file in UT2004/System folder.
     *
     * @throws cz.cuni.amis.pogamut.base.exceptions.PogamutException
     */
    @Override
    public void logic() throws PogamutException {

        for (AAction action : actions) {
            if (action.perform()) {
                break;
            }
        }
    }

    /**
     * This method is called when the bot is started either from IDE or from
     * command line.
     *
     * @param args 1. parametr: 2015 ... letosni rok a zaroven verze meho
     * skriptu 2. parametr: 0 nebo 1, kde 0 je cerveny tym a 1 je modry 3.
     * parametr: 0 az 7 je skill botu (planuji, ze v turnaji na konci semestru
     * bude nastaven na 74) 4. parametr: 3 ... to je pocet botu v tymu a letos
     * to bude vzdy 3 5. parametr: localhost nebo xx.xx.xx.xx ... IP adresa
     * serveru
     *
     */
    public static void main(String args[]) throws PogamutException {

        int team, skill, botCount;
        String serverIP;

//        if(args.length == 5) {
        try {
            team = Integer.parseInt(args[1]);
            skill = Integer.parseInt(args[2]);
            botCount = Integer.parseInt(args[3]);
            serverIP = args[4];
        } catch (Exception ignore) { // fallback
            team = 0;
            skill = 4;
            botCount = 3;
            serverIP = "localhost";
        }

        UT2004BotParameters[] bots = new UT2004BotParameters[botCount];
        for (int i = 0; i < bots.length; i++) {
            bots[i] = new CTFMasterParameters().setTeam(team).setSkill(skill);
        }

        TCServer = UT2004TCServer.startTCServer();
        new UT2004BotRunner(CTFMaster.class, "CTFMaster").setMain(true).startAgents(bots);
    }

    private void turnAround() {
        move.jump();
        move.turnHorizontal(90);
    }

    public RoleService getRoleService() {
        return roleService;
    }
    
    

    @Override
    protected void finalize() throws Throwable {
        TCServer.stop();
        super.finalize();
    }

}
