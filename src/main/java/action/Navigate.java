/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package action;

import ctfbot.CTFMaster;
import cz.cuni.amis.pogamut.base3d.worldview.object.ILocated;
import cz.cuni.amis.pogamut.base3d.worldview.object.Location;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.NavPoint;
import helper.DefenderPost;
import helper.EnemyMovementInfoService;
import helper.EnemyState;
import helper.StrategicPath;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import roles.Role;

/**
 *
 * @author root
 */
public class Navigate extends ResponsibleNavigation {

    private boolean goneForFlag;

    public Navigate(CTFMaster bot) {
        super(bot);
    }

//    public Item getNearestWeaponOrAmmo() {
//        Set<Item> weaponAndAmmoSet = new HashSet<Item>();
//        weaponAndAmmoSet.addAll(items.getAllItems(UT2004ItemType.Category.WEAPON).values());
//        weaponAndAmmoSet.addAll(items.getAllItems(UT2004ItemType.Category.AMMO).values());
//
//        return fwMap.getNearestFilteredItem(weaponAndAmmoSet, info.getNearestNavPoint(), new IFilter<Item>() {
//            @Override
//            public boolean isAccepted(Item object) {
//                object.getType();
//                if (items.isPickable(object) && items.isPickupSpawned(object)) {
//                    return wantedWeapons.contains(object) || wantedAmmo.contains(object);
//                } else {
//                    return false;
//                }
//            }
//        });
//    }
//
//    public Item getNearestHealth() {
//        return fwMap.getNearestFilteredItem(items.getAllItems(UT2004ItemType.Category.HEALTH).values(), info.getNearestNavPoint(), new IFilter<Item>() {
//            @Override
//            public boolean isAccepted(Item object) {
//                return (items.isPickable(object) && items.isPickupSpawned(object));
//            }
//        });
//    }
//
//    public Item getNearestSpecialItem() {
////        fwMap.get
//
//        return fwMap.getNearestFilteredItem(items.getAllItems().values(), info.getNearestNavPoint(), new IFilter<Item>() {
//            @Override
//            public boolean isAccepted(Item object) {
//                if (items.isPickable(object) && items.isPickupSpawned(object)) {
//                    return true;
//                } else {
//                    return false;
//                }
//            }
//        });
//    }
    /**
     * 
     * @param point
     * @param percentRangeToTakeItems how much farther in ratio may be the path through item point (from 0 to 0.5)
     */
    private void startSmartNavigation(ILocated point, double percentRangeToTakeItems) {
        if (point != null) {
            log.info("(( Navigate smart to: "+point.getLocation());
            goToLocation(point.getLocation(), Math.max(0, Math.min(0.5, percentRangeToTakeItems)));
        }
    }

    @Override
    protected boolean defenderLogic() {
        log.info("(( Defender logic called");
        if (defenderViewPoints.isEmpty()) {
            return supportLogic();
        } else {
            DefenderPost randomSnipingPoint = defenderViewPoints.get(bot.getRandom().nextInt(defenderViewPoints.size()));
            if (!botNav.isNavigating() && !randomSnipingPoint.isTaboo()) {
                startSmartNavigation(randomSnipingPoint.post, 0);
                //            botNav.navigate(randomSnipingPoint.post);
                botNav.setFocus(randomSnipingPoint.focus);
                randomSnipingPoint.inPosition();
            } else {
                goForNearestWeaponOrAmmo(500);
                
            }
            return true;
        }
    }

    @Override
    protected boolean leaderLogic() {
        log.info("(( Leader logic called");
        this.goneForFlag = !goneForFlag;
//        setTarget(realTarget);
        startSmartNavigation((!this.goneForFlag) ? ctf.getEnemyBase() : ctf.getOurBase(), 0.15);

        //        goForNearestWeaponOrAmmo((halfMapDistance / 10));
        return true;
    }

    @Override
    protected boolean supportLogic() {
        return leaderLogic();
    }

    /**
     * If enemies are known, find the path close to the enemy, but not exactly
     * enemy's. else random
     *
     * @return Path without enemy or random
     */
    public StrategicPath getSecurePathCloseToEnemy() {
        List<EnemyMovementInfoService> enemyOnPathLocations = getEnemyOnPathLocations();
        HashSet<StrategicPath> freePathsSet = new HashSet<StrategicPath>(strategicPaths);
        HashSet<StrategicPath> enemyPathsSet = new HashSet<StrategicPath>();
        HashSet<StrategicPath> goodPathsSet = new HashSet<StrategicPath>();

        for (EnemyMovementInfoService enemyOnPathInfoService : enemyOnPathLocations) {
            EnemyMovementInfoService.PathInfo pathInfo1 = enemyOnPathInfoService.getEnemyPathInfo();
            if (pathInfo1 != null) {
                StrategicPath enemyPath = pathInfo1.strategicPath;
                freePathsSet.remove(enemyPath);
                enemyPathsSet.add(enemyPath);
            }

            EnemyMovementInfoService.PathInfo pathInfo2 = enemyOnPathInfoService.getSecondNearestPathInfo();
            if (pathInfo2 != null) {
                goodPathsSet.add(pathInfo2.strategicPath);
            }
        }

        StrategicPath chosenPath;

        if (!goodPathsSet.isEmpty()) {
            chosenPath = goodPathsSet.iterator().next();
        } else if (!freePathsSet.isEmpty()) {
            chosenPath = freePathsSet.iterator().next();
        } else { // TODO: no time for better logic..
            chosenPath = strategicPaths.get(bot.getRandom().nextInt(strategicPaths.size()));
        }

        return chosenPath;
    }

    /**
     * If enemies are known, find the path that the enemy took, else get random
     * path
     *
     * @return Path with enemy or random
     */
    public StrategicPath getSamePathAsEnemy() {
        List<EnemyMovementInfoService> enemyOnPathLocations = getEnemyOnPathLocations();
        HashSet<StrategicPath> enemyPathsSet = new HashSet<StrategicPath>();

        for (EnemyMovementInfoService enemyOnPathInfoService : enemyOnPathLocations) {
            EnemyMovementInfoService.PathInfo pathInfo1 = enemyOnPathInfoService.getEnemyPathInfo();
            if (pathInfo1 != null) {
                StrategicPath enemyPath = pathInfo1.strategicPath;
                enemyPathsSet.add(enemyPath);
            }
        }

        StrategicPath chosenPath;

        // TODO: no time for better logic..
        if (!enemyPathsSet.isEmpty()) {
            chosenPath = enemyPathsSet.iterator().next();
        } else {
            chosenPath = strategicPaths.get(bot.getRandom().nextInt(strategicPaths.size()));
        }

        return chosenPath;
    }

    public List<EnemyMovementInfoService> getEnemyOnPathLocations() {
        Collection<EnemyState> knownEnemies = bot.getFightModule().getKnownEnemies();

        List<EnemyMovementInfoService> enemyInfoList = new ArrayList<EnemyMovementInfoService>();

        for (EnemyState enemy : knownEnemies) {
            if (enemy.isActual(bot.getSimTime())) {
                EnemyMovementInfoService movementInfoService = new EnemyMovementInfoService(enemy, fwMap, info);
                enemyInfoList.add(movementInfoService);
                for (StrategicPath path : strategicPaths) {
                    movementInfoService.countDistanceToPath(path);
                }
            }
        }

        return enemyInfoList;
    }

    /**
     * base layer, which should always do something, if requested
     *
     * @return true
     */
    @Override
    protected boolean isReady() {
        return true;
    }

    @Override
    protected boolean isBusy() {
        return realTarget != null;
    }

    @Override
    protected boolean continuePerforming() {
        if (!botNav.isNavigating() && realTarget != null) {
            log.info("!! starting navigation again");
            startSmartNavigation(realTarget, 0);
            return true;
        } else {
            return realTarget != null;
        }
    }

    /**
     * Plan should not change internally
     *
     * @return false
     */
    @Override
    protected boolean isPlanChanged() {
        return false;
    }

    @Override
    protected void targetReachedHandler() {
        if (realTarget != null) {
            NavPoint nearestNavPoint = info.getNearestNavPoint();
            log.info("(( current NavPoint is "+nearestNavPoint+" target is "+ realTargetNavPoint);
            if (nearestNavPoint.equals(realTargetNavPoint) || 
                    nearestNavPoint.getLocation().getDistance(realTarget) < 100 || 
                    realTarget.equals(lastTargetReached)) {
                log.info("Reached real destination on: " + realTarget);
                this.lastTargetReached = new Location(realTarget);
                notifyTargetReached(realTargetNavPoint);
                reset();
            } else {
                goToNextLocation();
            }
        }
    }

    @Override
    protected void handleAnotherPathElement() {
        if (bot.getFlagCaptureModule().getState() == FlagCapture.State.DEFENDING_CARRIER) {
            botNav.setFocus(info.getNearestNavPoint());
        } else if (bot.getRoleService().getRole() == Role.SUPPORT) {
        }
    }
}
