/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package action;

import communication.TCFlagSeen;
import ctfbot.BotState;
import ctfbot.CTFMaster;
import cz.cuni.amis.pogamut.base.communication.worldview.listener.annotation.AnnotationListenerRegistrator;
import cz.cuni.amis.pogamut.base.communication.worldview.listener.annotation.EventListener;
import cz.cuni.amis.pogamut.base.communication.worldview.listener.annotation.ObjectClassListener;
import cz.cuni.amis.pogamut.base.communication.worldview.object.IWorldObjectEvent;
import cz.cuni.amis.pogamut.base.communication.worldview.object.event.WorldObjectEvent;
import cz.cuni.amis.pogamut.base.communication.worldview.object.event.WorldObjectUpdatedEvent;
import cz.cuni.amis.pogamut.base3d.worldview.object.Location;
import cz.cuni.amis.pogamut.unreal.communication.messages.UnrealId;
import cz.cuni.amis.pogamut.ut2004.bot.impl.UT2004BotModuleController;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.FlagInfo;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.FlagInfoMessage;
import helper.FlagState;
import helper.NextFlagAction;
import helper.StrategicPath;

/**
 *
 * @author root
 */
public class FlagCapture extends AAction {

    private FlagState enemyFlagState;
    private FlagState ourFlagState;

    private State currentState;
    private NextFlagAction plannedAction;

    private StrategicPath chosenPath;

    public FlagCapture(CTFMaster bot) {
        super(bot);
        enemyFlagState = new FlagState(ctf.getEnemyFlag().getId());
        ourFlagState = new FlagState(ctf.getOurFlag().getId());
    }

    @ObjectClassListener(objectClass = FlagInfo.class)
    public void flagStateChanged(IWorldObjectEvent<FlagInfoMessage> event) {
        FlagInfoMessage flagInfo = event.getObject();
        Integer flagTeam = flagInfo.getTeam();

        FlagState.State lastState;
//        FlagState flagStateToUpdate = (flagTeam.equals(info.getTeam())) ? ;

        if (flagTeam != null) {

            if (flagTeam.equals(info.getTeam())) {
                handleOurFlagStateChange(flagInfo);
            } else {
                handleEnemyFlagStateChange(flagInfo);
            }

            notifyFlagSeen(event, flagInfo);
        }
    }

    protected void handleEnemyFlagStateChange(FlagInfoMessage flagInfo) {
        FlagState.State lastState;
        lastState = enemyFlagState.state;
        enemyFlagState.updateStateDirect(flagInfo);
        if (ourFlagState.state != lastState) {

        }
    }

    protected void handleOurFlagStateChange(FlagInfoMessage flagInfo) {
        FlagState.State lastState;
        lastState = ourFlagState.state;
        ourFlagState.updateStateDirect(flagInfo);
        if (ourFlagState.state != lastState) {

        }
    }

    private void notifyFlagSeen(IWorldObjectEvent event, FlagInfoMessage flagInfo) {
        if (event instanceof WorldObjectUpdatedEvent && flagInfo.isVisible()) {
            TCFlagSeen messageFlagSeen = new TCFlagSeen(flagInfo.getId(),
                    flagInfo.getLocation(), flagInfo.getTeam(),
                    FlagState.State.getFromString(flagInfo.getState()),
                    flagInfo.getHolder()
            );

            log.info(">> Notify flag seen at: " + messageFlagSeen.getLocation());

            com.sendToTeamOthers(messageFlagSeen);
        }
    }

    @EventListener(eventClass = TCFlagSeen.class)
    protected void flagInfoMessage(TCFlagSeen message) {
        log.info("<< I've been notified about flag at: " + message.getLocation());

        if (info.getTeam().equals(message.getTeam())) {
            ourFlagState.updateStateMediated(message);
        } else {
            enemyFlagState.updateStateMediated(message);
        }
    }

    @Override
    protected boolean defenderLogic() {
        return false;
    }

    @Override
    protected boolean leaderLogic() {
        return false;
    }

    @Override
    protected boolean supportLogic() {
        if (ourFlagState.state == FlagState.State.DROPPED && ourFlagState.isActual(info.getSelf().getSimTime())) {
            this.currentState = State.CHASING_OUR_FLAG;
        }

        return false;
    }

    protected boolean isPending() {
        switch (roleService.getRole()) {
            case LEADER:
                return isLeaderReady() || needsImmediateAction();
            default:
                return needsImmediateAction();
//            case DEFENDER:
//                return needsDefenderAction();
//            case SUPPORT:
//                return needsSupportAction();
        }
    }

    private boolean needsImmediateAction() {
        return enemyFlagState.state == FlagState.State.DROPPED
                || (ourFlagState.state == FlagState.State.HELD || ourFlagState.state == FlagState.State.DROPPED);
    }

    @Override
    public void reset() {
        this.currentState = State.GETTING_READY;
    }

    private boolean isLeaderReady() {
        return true;
    }

    @Override
    protected boolean isBusy() {
        return this.currentState != State.GETTING_READY;
    }

    @Override
    protected boolean continuePerforming() {
        return false;
    }

    @Override
    protected boolean isReady() {
        return needsImmediateAction() || bot.getState() == BotState.READY;
    }

    @Override
    protected boolean isPlanChanged() {
        return (plannedAction != null && plannedAction.isTimedOut());
    }

    public State getState() {
        return this.currentState;
    }

    public enum State {

        CARYING_FLAG,
        DEFENDING_CARRIER,
        CHASING_ENEMY_FLAG,
        CHASING_OUR_FLAG,
        GETTING_READY
    }
}
