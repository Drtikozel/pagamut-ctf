/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package action;

import ctfbot.CTFMaster;
import cz.cuni.amis.pogamut.base.communication.worldview.listener.annotation.AnnotationListenerRegistrator;
import cz.cuni.amis.pogamut.base3d.worldview.object.Location;
import cz.cuni.amis.pogamut.ut2004.agent.navigation.IUT2004Navigation;
import cz.cuni.amis.pogamut.ut2004.bot.impl.UT2004Bot;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.NavPoint;
import helper.AKnowledgeBase;
import java.util.HashSet;
import java.util.Set;
import roles.Role;

/**
 *
 * @author root
 */
public abstract class AAction extends AKnowledgeBase {

    protected static Set<AAction> targetReachedListenerRegister = new HashSet<AAction>();
    protected AnnotationListenerRegistrator listenerRegistrator;

    public AAction(CTFMaster bot) {
        super(bot);
        reset();
        initializeListeners(bot.getBot());
    }

    /**
     * Method that will be called after any target reached
     */
    protected void targetReachedInfo(NavPoint target) {}

    private void initializeListeners(UT2004Bot bot) {
        log.info("Initializing listener for " + this.getClass());
        listenerRegistrator = new AnnotationListenerRegistrator(this, bot.getWorldView(), bot.getLogger().getCategory("Listeners"));
        listenerRegistrator.addListeners();
        targetReachedListenerRegister.add(this);
    }

    public final boolean perform() {
        if (isBusy() && !isPlanChanged()) {
            return continuePerforming();
        } else {
            Role role = bot.getRoleService().getRole();
            if (role != null && isReady()) {
                switch (role) {
                    case DEFENDER:
                        return defenderLogic();
                    case LEADER:
                        return leaderLogic();
                    case SUPPORT:
                        return supportLogic();
                    default:
                        return false;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * Check if layer needs to do some action + prerequisities before action
     */
    protected abstract boolean isReady();

    /**
     * @return Is this layer already doing something?
     */
    protected abstract boolean isBusy();

    /**
     * Ensure that current plan is still actual
     * @return true if actual and need to continue
     */
    protected boolean continuePerforming() {
        return true;
    }

    protected abstract boolean isPlanChanged();

    public abstract void reset();

    /**
     * Executes defender logic. plan and perform next action
     *
     * @return false if nothing to do, true otherwise
     */
    protected abstract boolean defenderLogic();

    /**
     * Executes leader logic. plan and perform next action
     *
     * @return false if nothing to do, true otherwise
     */
    protected abstract boolean leaderLogic();

    /**
     * Executes support logic. plan and perform next action
     *
     * @return false if nothing to do, true otherwise
     */
    protected abstract boolean supportLogic();
}
