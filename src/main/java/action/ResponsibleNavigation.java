/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package action;

import communication.TCItemPicked;
import ctfbot.CTFMaster;
import cz.cuni.amis.pogamut.base.agent.navigation.IPathExecutorState;
import cz.cuni.amis.pogamut.base.communication.worldview.listener.annotation.EventListener;
import cz.cuni.amis.pogamut.base3d.worldview.object.ILocated;
import cz.cuni.amis.pogamut.base3d.worldview.object.Location;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensor.AgentInfo;
import cz.cuni.amis.pogamut.ut2004.agent.module.utils.TabooSet;
import cz.cuni.amis.pogamut.ut2004.communication.messages.UT2004ItemType;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.Item;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.ItemPickedUp;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.NavPoint;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.Player;
import cz.cuni.amis.utils.IFilter;
import cz.cuni.amis.utils.exception.PogamutException;
import cz.cuni.amis.utils.flag.FlagListener;
import helper.DefenderPost;
import helper.MeetPoint;
import helper.StrategicPath;
import helper.WantedWeapon;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author root
 */
public abstract class ResponsibleNavigation extends AAction implements FlagListener<IPathExecutorState> {

    protected Location lastTargetReached = null;

    protected Location realTarget;
    protected Location nextTarget;

    protected NavPoint realTargetNavPoint;
    protected NavPoint nextTargetNavPoint;

    protected int stuckCounter = 0;

    List<DefenderPost> defenderViewPoints = new ArrayList<DefenderPost>();
//    List<MeetPoint> meetPoints = new ArrayList<MeetPoint>();
    List<StrategicPath> strategicPaths = new ArrayList<StrategicPath>();

    protected static double halfMapDistance = Double.MAX_VALUE;
    private double percentRangeToTakeItems = 0.0;

    protected Set<WantedWeapon> wantedWeapons = new HashSet<WantedWeapon>(Arrays.asList(
            new WantedWeapon(UT2004ItemType.LIGHTNING_GUN, UT2004ItemType.LIGHTNING_GUN_AMMO),
            new WantedWeapon(UT2004ItemType.LINK_GUN, UT2004ItemType.LINK_GUN_AMMO),
            new WantedWeapon(UT2004ItemType.ASSAULT_RIFLE, UT2004ItemType.ASSAULT_RIFLE_AMMO),
            new WantedWeapon(UT2004ItemType.FLAK_CANNON, UT2004ItemType.FLAK_CANNON_AMMO),
            new WantedWeapon(UT2004ItemType.MINIGUN, UT2004ItemType.MINIGUN_AMMO),
            new WantedWeapon(UT2004ItemType.SHOCK_RIFLE, UT2004ItemType.SHOCK_RIFLE_AMMO)
    ));

    private TabooSet<Item> tabooItems = new TabooSet<Item>(bot.getBot());

    public ResponsibleNavigation(CTFMaster bot) {
        super(bot);
        halfMapDistance = fwMap.getDistance(ctf.getEnemyBase(), ctf.getEnemyBase());
        botNav.getPathExecutor().getState().addStrongListener(this);
        initInterestingPoints();
    }

    public static double getHalfMapDistance() {
        return Navigate.halfMapDistance;
    }

    protected final void initInterestingPoints() {
        String mapLowerCase = game.getMapName().toLowerCase();
        if (mapLowerCase.contains("citadel")) {
            citadelPoints();
        } else if (mapLowerCase.contains("concentrate")) {
            concentratePoints();
        }
    }

    protected void concentratePoints() {
        if (info.getTeam() == AgentInfo.TEAM_RED) {
            concentrateRedTeamPoints();
        } else {
            concentrateBlueTeamPoints();
        }
    }

    protected void citadelPoints() {
        if (info.getTeam() == AgentInfo.TEAM_RED) {
            citadelRedTeamPoints();
        } else {
            citadelBlueTeamPoints();
        }
    }

    protected void citadelRedTeamPoints() {
        defenderViewPoints.add(new DefenderPost(navPoints.getNavPoint("CTF-Citadel.PathNode14"), ctf.getEnemyBase()));
        defenderViewPoints.add(new DefenderPost(navPoints.getNavPoint("CTF-Citadel.PathNode10"), ctf.getEnemyBase()));

        strategicPaths.add(new StrategicPath(
                new MeetPoint((navPoints.getNavPoint("CTF-Citadel.PathNode5"))),
                Arrays.asList(
                        navPoints.getNavPoint("CTF-Citadel.PathNode106")
                )
        ));

        strategicPaths.add(new StrategicPath(
                new MeetPoint((navPoints.getNavPoint("CTF-Citadel.PathNode30"))),
                Arrays.asList(
                        navPoints.getNavPoint("CTF-Citadel.PathNode25")
                )
        ));

        strategicPaths.add(new StrategicPath(
                new MeetPoint((navPoints.getNavPoint("CTF-Citadel.PathNode41"))),
                Arrays.asList(
                        navPoints.getNavPoint("CTF-Citadel.PathNode47")
                )
        ));
    }

    protected void citadelBlueTeamPoints() {
        defenderViewPoints.add(new DefenderPost(navPoints.getNavPoint("CTF-Citadel.PathNode99"), ctf.getEnemyBase()));
        defenderViewPoints.add(new DefenderPost(navPoints.getNavPoint("CTF-Citadel.PathNode36"), ctf.getEnemyBase()));

        strategicPaths.add(new StrategicPath(
                new MeetPoint((navPoints.getNavPoint("CTF-Citadel.PathNode3"))),
                Arrays.asList(
                        navPoints.getNavPoint("CTF-Citadel.PathNode106")
                )
        ));

        strategicPaths.add(new StrategicPath(
                new MeetPoint((navPoints.getNavPoint("CTF-Citadel.PathNode11"))),
                Arrays.asList(
                        navPoints.getNavPoint("CTF-Citadel.PathNode25")
                )
        ));

        strategicPaths.add(new StrategicPath(
                new MeetPoint((navPoints.getNavPoint("CTF-Citadel.PathNode75"))),
                Arrays.asList(
                        navPoints.getNavPoint("CTF-Citadel.PathNode47")
                )
        ));
    }

    protected void concentrateRedTeamPoints() {
        NavPoint defenderPost = navPoints.getNavPoint("CTF-BP2-Concentrate.PathNode86");

        defenderViewPoints.add(new DefenderPost(defenderPost, navPoints.getNavPoint("CTF-BP2-Concentrate.InventorySpot1")));
        defenderViewPoints.add(new DefenderPost(defenderPost, navPoints.getNavPoint("CTF-BP2-Concentrate.InventorySpot2")));

        MeetPoint meetPoint = new MeetPoint((navPoints.getNavPoint("CTF-BP2-Concentrate.InventorySpot29")));
        NavPoint doubleDamagePoint = navPoints.getNavPoint("CTF-BP2-Concentrate.AssaultPath15");

        strategicPaths.add(new StrategicPath(
                meetPoint,
                Arrays.asList(
                        doubleDamagePoint,
                        navPoints.getNavPoint("CTF-BP2-Concentrate.AssaultPath7") // podchod
                )
        ));

        strategicPaths.add(new StrategicPath(
                meetPoint,
                Arrays.asList(
                        doubleDamagePoint,
                        navPoints.getNavPoint("CTF-BP2-Concentrate.InventorySpot64") // nadchod
                )
        ));

    }

    protected void concentrateBlueTeamPoints() {
        NavPoint defenderPost = navPoints.getNavPoint("CTF-BP2-Concentrate.PathNode5");

        defenderViewPoints.add(new DefenderPost(defenderPost, navPoints.getNavPoint("CTF-BP2-Concentrate.InventorySpot1")));
        defenderViewPoints.add(new DefenderPost(defenderPost, navPoints.getNavPoint("CTF-BP2-Concentrate.InventorySpot2")));

        MeetPoint meetPoint = new MeetPoint((navPoints.getNavPoint("CTF-BP2-Concentrate.InventorySpot36")));
        NavPoint doubleDamagePoint = navPoints.getNavPoint("CTF-BP2-Concentrate.AssaultPath15");

        strategicPaths.add(new StrategicPath(
                meetPoint,
                Arrays.asList(
                        doubleDamagePoint,
                        navPoints.getNavPoint("CTF-BP2-Concentrate.AssaultPath14") // podchod
                )
        ));

        strategicPaths.add(new StrategicPath(
                meetPoint,
                Arrays.asList(
                        doubleDamagePoint,
                        navPoints.getNavPoint("CTF-BP2-Concentrate.PathNode60") // nadchod
                )
        ));
    }

    public void setFocus(ILocated target) {
        botNav.setFocus(target);
    }

    @Override
    public void flagChanged(IPathExecutorState changedValue) {
        switch (changedValue.getState()) {
            case PATH_COMPUTATION_FAILED:
                reset();
                break;
            case STUCK:
                stuckHandler();
                break;
            case TARGET_REACHED:
                targetReachedHandler();
                break;
            case STOPPED:
                break;
            case SWITCHED_TO_ANOTHER_PATH_ELEMENT:
                handleAnotherPathElement();
                break;
        }
    }

    protected abstract void targetReachedHandler();

    protected abstract void handleAnotherPathElement();

    private void stuckHandler() {
        if (this.stuckCounter++ >= 3) {
            NavPoint currentTargetNavPoint = botNav.getCurrentTargetNavPoint();
            if (currentTargetNavPoint == null) {
                reset();
                log.info("!! Bot stucked");
            } else {
                NavPoint myNearestNavPoint = info.getNearestNavPoint();

                if (!myNearestNavPoint.equals(currentTargetNavPoint) && fwMap.reachable(myNearestNavPoint, currentTargetNavPoint)) {
                    botNav.navigate(currentTargetNavPoint);
                    log.info("!! Bot stucked, navigating to navpoint");
                } else {
                    reset();
                    log.info("!! Bot stucked, reset");
                }
            }

            this.stuckCounter = 0;
        }
    }

//    public ILocated getRealTarget() {
//        return realTarget;
//    }
//
//    public Location getNextTarget() {
//        return nextTarget;
//    }

    private void setNextTarget(Location nextTarget) {
        this.nextTarget = nextTarget;
        if (nextTarget != null) {
            this.nextTargetNavPoint = info.getNearestNavPoint(nextTarget);

            if (this.nextTargetNavPoint == null) {
                this.nextTarget = null;
            }
        } else {
            this.nextTargetNavPoint = null;
        }
    }

    private void setRealTarget(Location realTarget) {
        this.realTarget = realTarget;
        if (realTarget != null) {
            this.realTargetNavPoint = info.getNearestNavPoint(realTarget);

            if (this.realTargetNavPoint == null) {
                this.realTarget = null;
            }
        } else {
            this.realTargetNavPoint = null;
        }
    }

    /**
     * Tell the navigation to start navigating to some point
     * @param target
     * @param percentRangeToTakeItems how much farther may be the path through item
     */
    protected void goToLocation(Location target, double percentRangeToTakeItems) {
        this.percentRangeToTakeItems = percentRangeToTakeItems;
        this.setRealTarget(target);
        if (info.getNearestNavPoint().equals(realTargetNavPoint)) {
            log.info(">< I'm already here " + target.getLocation());
            notifyTargetReached(realTargetNavPoint);
            reset();
        } else {
            goToNextLocation();
        }
    }

    /**
     *
     */
    protected void goToNextLocation() {
        NavPoint nearestNavPoint = info.getNearestNavPoint();
        this.setNextTarget(null);

        if (realTarget != null) {
            log.info(")) I have real target: "+realTarget.getLocation());
            if (percentRangeToTakeItems > 0.0) {
                Item itemOnPath = findItemOnPath(nearestNavPoint, this.realTargetNavPoint);

                if (itemOnPath != null) {
                    this.setNextTarget(itemOnPath.getLocation());
                }
            }

            if (this.nextTarget != null) {
                log.info("--> Will go for item " + this.nextTarget.getLocation());
                botNav.navigate(nextTarget);
            } else {
                log.info("--> Will go to target " + realTarget.getLocation());
                botNav.navigate(realTarget);
            }
        }
    }

    @Override
    public void reset() {
        botNav.stopNavigation();
        botNav.setFocus(null);
        this.setNextTarget(null);
        this.setRealTarget(null);
        this.lastTargetReached = null;
    }

    protected Item findItemOnPath(final NavPoint pathFrom, final NavPoint pathTo) {
        //        TreeMap<Double, Item> itemsMap = new TreeMap<Double, Item>();
        if (pathFrom != null && pathTo != null) {
            return fwMap.getNearestFilteredItem(items.getSpawnedItems().values(), pathFrom, new IFilter<Item>() {
                @Override
                public boolean isAccepted(Item item) {
                    return isWantedItemOnPath(item, pathFrom, pathTo);
                }
            });
        } else {
            return null;
        }
        //        return itemsMap;
    }

//        public void goForNearestWantedItem() {
//    
//            if (!botNav.isNavigating()) {
//                Item nearestItem;
//                Item nearestSpecialItem = getNearestSpecialItem();
//                Item nearestWeaponOrAmmo = getNearestWeaponOrAmmo();
//    
//                Double distanceToWeapon = (nearestWeaponOrAmmo != null)
//                        ? fwMap.getDistance(info.getNearestNavPoint(), nearestWeaponOrAmmo.getNavPoint())
//                        : -1;
//    
//                Double distanceToSpecial = (nearestSpecialItem != null)
//                        ? fwMap.getDistance(info.getNearestNavPoint(), nearestSpecialItem.getNavPoint())
//                        : -1;
//    
//                if (distanceToSpecial > 0 && distanceToSpecial <= distanceToWeapon) {
//                    botNav.navigate(nearestSpecialItem);
//                    log.info("Will go for special item");
//                } else if (distanceToWeapon > 0) {
//                    botNav.navigate(nearestWeaponOrAmmo);
//                    log.info("Will go for weapon");
//                } else if (info.isHealthy()) {
//                    Item nearestHealth = getNearestHealth();
//                    if (nearestHealth != null) {
//                        botNav.navigate(nearestHealth);
//                        log.info("Will go for life");
//                    }
//                }
//            }
//        }
        
    public void follow(Player p) {
        botNav.navigate(p);
    }

    protected Item getNearestAmmo() {
        double nearestAmmoDistance = Double.MAX_VALUE;
        Item nearestAmmo = null;
        for (WantedWeapon weapon : wantedWeapons) {
            if (weaponry.hasWeapon(weapon.weapon)) {
                Item item = fwMap.getNearestItem(items.getSpawnedItems(weapon.primaryAmmo).values(), info.getNearestNavPoint());
                if (item != null && weaponry.hasLowAmmoForWeapon(weapon.weapon, 0.2)) {
                    double distance = fwMap.getDistance(info.getNearestNavPoint(), info.getNearestNavPoint(item.getLocation()));
                    if (nearestAmmoDistance > distance) {
                        nearestAmmo = item;
                        nearestAmmoDistance = distance;
                    }
                }
            }
        }
        return nearestAmmo;
    }

    protected Item getNearestWeapon() {
        double nearestWeaponDistance = Double.MAX_VALUE;
        Item nearestWeapon = null;
        for (WantedWeapon weapon : wantedWeapons) {
            if (!weaponry.hasWeapon(weapon.weapon)) {
                Item item = fwMap.getNearestItem(items.getSpawnedItems(weapon.weapon).values(), info.getNearestNavPoint());
                if (item != null) {
                    double distance = fwMap.getDistance(info.getNearestNavPoint(), info.getNearestNavPoint(item.getLocation()));
                    if (nearestWeaponDistance > distance) {
                        nearestWeapon = item;
                        nearestWeaponDistance = distance;
                    }
                }
            }
        }
        return nearestWeapon;
    }

    protected void goForNearestWeaponOrAmmo(double range) {
        Item nearestWeapon = getNearestWeapon();
        if (nearestWeapon != null && fwMap.getDistance(info.getNearestNavPoint(), info.getNearestNavPoint(nearestWeapon)) < range) {
            botNav.navigate(nearestWeapon);
            log.info("++ Go for nearest weapon");
        } else {
            Item nearestAmmo = getNearestAmmo();
            if (nearestAmmo != null && fwMap.getDistance(info.getNearestNavPoint(), info.getNearestNavPoint(nearestAmmo)) < range) {
                botNav.navigate(nearestAmmo);
                log.info("++ Go for nearest ammo");
            }
        }
    }

    protected boolean isWantedItemOnPath(Item item, NavPoint pathFrom, NavPoint pathTo) {
        double distanceToTarget = fwMap.getDistance(pathFrom, pathTo);
        if (isItemTypeAcceptable(item)) {
            double distStartItem;
            double distItemDestination;
            distStartItem = fwMap.getDistance(pathFrom, item.getNavPoint());
            distItemDestination = fwMap.getDistance(item.getNavPoint(), pathTo);
            double pathDistDiff = Math.abs(distanceToTarget - distStartItem - distItemDestination);
            return (distItemDestination < distanceToTarget) && isItemAcceptable(item, pathDistDiff, distanceToTarget);
        } else {
            return false;
        }
    }

    /**
     * Inform other bots about item picked
     *
     * @param event item picked up event
     */
    @EventListener(eventClass = ItemPickedUp.class)
    public void itemPickedUpEvent(ItemPickedUp event) {
        if (!event.isDropped()) {
            tabooItems.add(items.getItem(event.getId()), 30);
            com.sendToTeamOthers(new TCItemPicked(event.getId()));
        }
    }

    /**
     * Listener for messages from other team players about taboo items.
     *
     * @param message picked item message
     */
    @EventListener(eventClass = TCItemPicked.class)
    public void itemPickedUpMessage(TCItemPicked message) {
        tabooItems.add(items.getItem(message.getId()), 30);
    }

    /**
     * @param item
     * @param pathDistDiff
     * @param distanceToTarget
     * @return Item is not farther than some percent of path lenght
     */
    protected boolean isItemAcceptable(Item item, double pathDistDiff, double distanceToTarget) {
        return pathDistDiff < (distanceToTarget * this.percentRangeToTakeItems);
    }

    /**
     * Checks if item is desired
     *
     * @param item
     * @return boolean
     */
    protected boolean isItemTypeAcceptable(Item item) {
        if (items.isPickable(item) && !tabooItems.isTaboo(item)) {
            switch (item.getType().getCategory()) {
                case AMMO:
                case WEAPON:
                    return wantedWeapons.contains(item.getType());
                case HEALTH:
                case SHIELD:
                case ARMOR:
                case ADRENALINE:
                    return true;
                default:
                    return false;
            }
        } else {
            return false;
        }
    }

    protected void notifyTargetReached(NavPoint target) {
        for (AAction module : targetReachedListenerRegister) {
            module.targetReachedInfo(target);
        }
    }

}
