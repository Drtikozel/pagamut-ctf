/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package action;

import communication.TCEnemySeen;
import ctfbot.CTFMaster;
import cz.cuni.amis.pogamut.base.communication.worldview.listener.annotation.EventListener;
import cz.cuni.amis.pogamut.base.communication.worldview.listener.annotation.ObjectClassListener;
import cz.cuni.amis.pogamut.base.communication.worldview.object.IWorldObjectEvent;
import cz.cuni.amis.pogamut.base.communication.worldview.object.WorldObjectId;
import cz.cuni.amis.pogamut.base3d.worldview.object.ILocated;
import cz.cuni.amis.pogamut.base3d.worldview.object.Location;
import cz.cuni.amis.pogamut.base3d.worldview.object.Rotation;
import cz.cuni.amis.pogamut.unreal.communication.messages.UnrealId;
import cz.cuni.amis.pogamut.ut2004.communication.messages.UT2004ItemType;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.BotDamaged;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.HearNoise;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.Player;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.PlayerKilled;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.PlayerMessage;
import cz.cuni.amis.utils.Cooldown;
import helper.EnemyState;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author root
 */
public class Fight extends AAction {

    private EnemyState enemyToKill;

//    private TreeMap<UnrealId, EnemyState> enemiesToKill;
    private final ConcurrentHashMap<WorldObjectId, EnemyState> knownEnemies;

    private FightState fightingState;
    private final int COOLDOWN = 5000;
    private final Cooldown lostFocusCooldown = new Cooldown(COOLDOWN);

    private final int CLOSE_COMBAT_RANGE = 300;
    private final int MEDIUM_COMBAT_RANGE = 800;
    private final int LONG_COMBAT_RANGE = 1200;
    private final int VERY_LONG_COMBAT_RANGE = 2000;
    private TreeMap<Double, EnemyState> currentVisibleEnemies;

    public Fight(CTFMaster bot) {
        super(bot);
        // thread-safe, sorted, iterable map
        knownEnemies = new ConcurrentHashMap<WorldObjectId, EnemyState>();

        this.initWeaponPrefs();
    }

    private void initWeaponPrefs() {
        shoot.setChangeWeaponCooldown(2000);

        weaponPrefs.addGeneralPref(UT2004ItemType.MINIGUN, true);
        weaponPrefs.addGeneralPref(UT2004ItemType.MINIGUN, false);
        weaponPrefs.addGeneralPref(UT2004ItemType.LINK_GUN, false);

        weaponPrefs.newPrefsRange(CLOSE_COMBAT_RANGE)
                .add(UT2004ItemType.FLAK_CANNON, true)
                .add(UT2004ItemType.LINK_GUN, true);
        weaponPrefs.newPrefsRange(MEDIUM_COMBAT_RANGE)
                .add(UT2004ItemType.MINIGUN, true)
                .add(UT2004ItemType.SHOCK_RIFLE, true)
                .add(UT2004ItemType.LINK_GUN, true);
        weaponPrefs.newPrefsRange(LONG_COMBAT_RANGE)
                .add(UT2004ItemType.MINIGUN, false)
                .add(UT2004ItemType.LIGHTNING_GUN, true);
        weaponPrefs.newPrefsRange(VERY_LONG_COMBAT_RANGE)
                .add(UT2004ItemType.LINK_GUN, false)
                .add(UT2004ItemType.LIGHTNING_GUN, true);

        // kratka vzdalenost linkgun nebo flakcannon (300)
        // větší (800) linkgun, minigun, shock rifle (lighting gun - při přebíjení změnit zbraň)
        // 1200 lightning gun, minigun (secondary)
        // 2000 linkgun (sec)
//            weaponPrefs.newPrefsRange(1000).add(); // nastavím si zbraně pro vzdálenosti 
//            shoot.setChangeWeaponCooldown(2000); (problém s hranicí, kdy mění zbraň
//            weaponPrefs.addGeneralPref(null, boolProp) // když se nevejdu do range
    }

    public boolean dumbKill(Player p) {
        if (p != null) {
//            botNav.set
            return (shoot.shoot(weaponPrefs, p) != null);
        } else {
            return false;
        }
    }

    @EventListener(eventClass = BotDamaged.class)
    public void botDamaged(BotDamaged event) {
        if (event.isDirectDamage() && !event.isCausedByWorld()) {
            if (this.fightingState != FightState.SHOOTING) {
                log.info("Ow! Stop pissing me off!");
                enemyAlert();
            }
        }
    }

    protected void enemyAlert() {
        if(this.fightingState != FightState.SHOOTING) {
            this.fightingState = FightState.RED_ALERT;
            this.lostFocusCooldown.use();
        }
    }
    
    @ObjectClassListener(objectClass = Player.class)
    public void enemyStateChange(IWorldObjectEvent<PlayerMessage> event) {
        PlayerMessage p = event.getObject();
        if (isEnemy(p)) {
            EnemyState enemy = getEnemyById(p.getId());
            enemy.updateStateDirect(p);

            if (p.isVisible()) {
                com.sendToTeamOthers(new TCEnemySeen(p.getId(), p.getLocation()));
                log.info(">> Can see enemy at: " + p.getLocation());
            } else if (isChasing(p)) {
                if (chill()) {
                    enemyToKill = null;
                }
            }
        }
    }

    protected boolean isChasing(Player p) {
        return isChasing(p.getId());
    }

    protected boolean isChasing(WorldObjectId id) {
        return enemyToKill != null && enemyToKill.id.equals(id);
    }

    @EventListener(eventClass = PlayerKilled.class)
    public void enemyKilled(PlayerKilled event) {
        UnrealId id = event.getId();
        if (isChasing(id)) {
            reset();
            log.info("|| Enemy was killed");
        }

        EnemyState enemy = getEnemyById(id);
        enemy.setLocation(Location.NONE);
    }

    protected EnemyState getEnemyById(WorldObjectId id) {
        EnemyState enemy = knownEnemies.get(id);
        if (enemy == null) {
            enemy = new EnemyState(id);
            knownEnemies.put(id, enemy);
        }

        return enemy;
    }

    protected Player getEnemyFromState(EnemyState state) {
        if (state != null) {
            return players.getPlayer((UnrealId) state.id);
        } else {
            return null;
        }
    }

    @EventListener(eventClass = TCEnemySeen.class)
    public void enemySeenMessage(TCEnemySeen message) {
        EnemyState enemy = getEnemyById(message.id);

        enemy.updateStateMediated(message);
        enemy.setVisibility(visibility.isVisible(enemy.location));

        log.info("<< Friend seen an enemy at: " + enemy.getLocation() + enemy.isVisible);
    }

    public Collection<EnemyState> getKnownEnemies() {
        return knownEnemies.values();
    }
    
    public TreeMap<Double, EnemyState> getVisibleEnemies() {
        EnemyState nearestEnemy = null;
        double minDistance = Double.MAX_VALUE;
        
        TreeMap<Double, EnemyState> visibleEnemies = new TreeMap<Double, EnemyState>();

        for (EnemyState enemy : knownEnemies.values()) {
            if (!enemy.isActual(info.getSelf().getSimTime())) {
                continue;
            }

            // set if bot can actually see the enemy from it's location
            if(visibility.isInitialized()) {
                enemy.setVisibility(visibility.isVisible(enemy.getLocation()));
            }

            if (enemy.isVisible()) {
                double dist = info.getLocation().getDistance(enemy.location);

                visibleEnemies.put(dist, enemy);
            }
        }

        return visibleEnemies;
    }

    /**
     * Checks if the last fighting state is cooled if bot shooted last time,
     * switch to RedAlert with cooldown else if cooldown timer expired, switch
     * to cool mode
     *
     * @return is everything cool?
     */
    protected boolean chill() {
        switch (fightingState) {
            case SHOOTING:
                shoot.stopShooting();
                enemyAlert();
                return false;
            case RED_ALERT:
                if (lostFocusCooldown.isCool()) {
                    reset();
                    return true;
                } else {
                    return false;
                }
            default:
                return true;
        }
    }

    @Override
    public void reset() {
        // hack..
        if (lostFocusCooldown != null) {
            lostFocusCooldown.clear();
        }
        
        botNav.setFocus(null);

        fightingState = FightState.COOL;
        enemyToKill = null;
    }

    @Override
    protected boolean defenderLogic() {
        return dumbKill(getEnemyFromState(this.currentVisibleEnemies.firstEntry().getValue()));
    }

    @Override
    protected boolean leaderLogic() {
        return dumbKill(getEnemyFromState(this.currentVisibleEnemies.firstEntry().getValue()));
    }

    @Override
    protected boolean supportLogic() {
        return dumbKill(getEnemyFromState(this.currentVisibleEnemies.firstEntry().getValue()));
    }

    @Override
    protected boolean isReady() {
        this.currentVisibleEnemies = getVisibleEnemies();
        
        if((!currentVisibleEnemies.isEmpty())) {
            log.info("Seen some enemies: "+currentVisibleEnemies.values().toString());
            return false;
        } else {
            return false;
        }
    }

    @Override
    protected boolean isBusy() {
        return (fightingState != FightState.COOL);
    }

    @Override
    protected boolean continuePerforming() {
        return (fightingState == FightState.SHOOTING) || !chill();
    }

    @Override
    protected boolean isPlanChanged() {
        return false;
    }

    protected enum FightState {
        SHOOTING,
        RED_ALERT,
        COOL;
    }

}

//    @EventListener(eventClass = HearNoise.class) // zatím nepoužíváme..
//public void heardNoise(HearNoise event) {
//        if (event.getType().toLowerCase().contains("weapon")) {
//            double noiseOn = (event.getRotation().yaw); // 65535.0
//            double currentRotation = info.getHorizontalRotation().yaw;
//            double turnBy = ((noiseOn - currentRotation) / 65535.0) * 360;
//            log.info("Heard noise, turning by " + currentRotation);
//            
//            Location currentFocus, newFocus, currentPos;
//            if(botNav.getFocus() == null) {
//                List<ILocated> currentPathDirect = botNav.getCurrentPathDirect();
//                if(currentPathDirect != null && currentPathDirect.size() >= 2) {
//                    currentFocus = currentPathDirect.get(0).getLocation();
//                    currentPos = info.getNearestNavPoint().getLocation();
//                    if(currentFocus.equals(currentPos)) {
//                        currentFocus = currentPathDirect.get(1).getLocation();
//                    }
//                    
//                    Location normalizedLocation = currentFocus.getLocation().sub(currentPos);
//                }
//            } 
//
//            botNav.setFocus(info);
//            
//            move.turnHorizontal((int) turnBy);
//        }
//    }
