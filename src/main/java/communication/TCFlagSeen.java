/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package communication;

import cz.cuni.amis.pogamut.base.communication.worldview.object.WorldObjectId;
import cz.cuni.amis.pogamut.base3d.worldview.object.Location;
import cz.cuni.amis.pogamut.unreal.communication.messages.UnrealId;
import cz.cuni.amis.utils.token.Tokens;
import helper.FlagState;

/**
 *
 * @author root
 */
public class TCFlagSeen extends TCObjectSeen {
    public int team;
    public FlagState.State state;
    public UnrealId holder;

    public TCFlagSeen(WorldObjectId id, Location location, int team, FlagState.State state, UnrealId holder) {
        super(Tokens.get("TCFlagSeen"), id, location);
        this.team = team;
        this.state = state;
        this.holder = holder;
    }

    public int getTeam() {
        return team;
    }

    public FlagState.State getState() {
        return state;
    }

    public UnrealId getHolder() {
        return holder;
    }
    
    
}
