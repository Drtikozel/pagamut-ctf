/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package communication;

import action.FlagCapture;
import cz.cuni.amis.pogamut.base.communication.worldview.object.WorldObjectId;
import cz.cuni.amis.pogamut.base3d.worldview.object.Location;
import cz.cuni.amis.pogamut.unreal.communication.messages.UnrealId;
import cz.cuni.amis.pogamut.ut2004.teamcomm.mina.messages.TCMessageData;
import cz.cuni.amis.utils.token.Tokens;
import helper.FlagState;

/**
 *
 * @author root
 */
public class TCFlagAction extends TCMessageData {
    public FlagCapture.State state;
    public final Location botLocation;

    public TCFlagAction(WorldObjectId id, Location botLocation, FlagCapture.State state) {
        super(Tokens.get("TCFlagAction"));
        this.state = state;
        this.botLocation = botLocation;
    }

    public FlagCapture.State getPlannedAction() {
        return state;
    }

    public Location getBotLocation() {
        return botLocation;
    }

}
