/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package helper;

import communication.TCFlagSeen;
import cz.cuni.amis.pogamut.base.communication.worldview.object.WorldObjectId;
import cz.cuni.amis.pogamut.base3d.worldview.object.Location;
import cz.cuni.amis.pogamut.unreal.communication.messages.UnrealId;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.FlagInfoMessage;

/**
 *
 * @author root
 */
public class FlagState extends WorldObjectState<FlagInfoMessage, TCFlagSeen> {

    public State state;
    public UnrealId holder;

    public FlagState(WorldObjectId id) {
        super(id);
        this.state = State.NOT_KNOWN;
    }
    
//    private static final int UP_TO_DATE_INFO_DELAY = 3000;

    @Override
    protected void updateStateDirectOptional(FlagInfoMessage message) {
        this.state = State.getFromString(message.getState());
        this.holder = message.getHolder();
    }

    @Override
    protected void updateStateMediatedOptional(TCFlagSeen message) {
        this.state = message.getState();
        this.holder = message.getHolder();
    }

    @Override
    protected int getMaxUpdateDelay() {
        return 3000;
    }

    @Override
    protected Location getLocationFromMessage(FlagInfoMessage message) {
        return message.getLocation();
    }

    @Override
    public boolean getVisibilityFromMessage(FlagInfoMessage message) {
        return message.isVisible();
    }
    
    /**
     * State by string message. Fuj..
     *
     * @param state status message
     * @return enum state
     */
    public enum State {

        HOME, HELD, DROPPED, NOT_KNOWN;

        public static State getFromString(String state) {
            if (state != null) {
                String stateLower = state.toLowerCase();

                if (stateLower.contains("home")) {
                    return State.HOME;
                } else if (stateLower.contains("dropped")) {
                    return State.DROPPED;
                } else if (stateLower.contains("held")) {
                    return State.HELD;
                } else {
                    return State.NOT_KNOWN;
                }
            } else {
                return State.NOT_KNOWN;
            }
        }
    }
}
