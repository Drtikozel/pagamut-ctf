/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package helper;

import cz.cuni.amis.pogamut.ut2004.bot.params.UT2004BotParameters;

/**
 *
 * @author root
 */
public class CTFMasterParameters extends UT2004BotParameters {
    private int skill;

    public CTFMasterParameters setSkill(int skill) {
        this.skill = skill;
        return this;
    }

    public int getSkill() {
        return this.skill;
    }

    @Override
    public CTFMasterParameters setTeam(Integer team) {
        super.setTeam(team);
        return this;
    }

}
