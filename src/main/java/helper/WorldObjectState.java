/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package helper;

import communication.TCObjectSeen;
import cz.cuni.amis.pogamut.base.communication.translator.event.IWorldObjectUpdatedEvent;
import cz.cuni.amis.pogamut.base.communication.worldview.object.WorldObjectId;
import cz.cuni.amis.pogamut.base3d.worldview.object.Location;
import cz.cuni.amis.utils.exception.PogamutException;

/**
 *
 * @author root
 * @param <WORLD_MESSAGE> Message received from world
 * @param <TEAM_MESSAGE> Message received from other bot
 */
public abstract class WorldObjectState<WORLD_MESSAGE extends IWorldObjectUpdatedEvent, TEAM_MESSAGE extends TCObjectSeen> 
implements Comparable<WorldObjectId>{

    public final WorldObjectId id;
    public Location location;
    public boolean isVisible;
    public long lastUpdated = 0;

    public WorldObjectState(WorldObjectId id) {
//        this();
        this.id = id;
        this.location = Location.NONE;
        this.isVisible = false;
    }

    public final synchronized void updateStateDirect(WORLD_MESSAGE message) {
        if (!this.id.equals(message.getId())) {
            throw new PogamutException("Updating wrong object", this);
        }

        this.lastUpdated = message.getSimTime();
        setLocation(getLocationFromMessage(message));
        setVisibility(getVisibilityFromMessage(message));
        
        updateStateDirectOptional(message);
    }
    protected void updateStateDirectOptional(WORLD_MESSAGE message) {}

    public final synchronized void updateStateMediated(TEAM_MESSAGE message) {
        if(!isVisible) {
            setLocation(message.getLocation());
            lastUpdated = message.getSimTime();
            
            updateStateMediatedOptional(message);
        }
    }
    protected void updateStateMediatedOptional(TEAM_MESSAGE message) {}

    protected abstract Location getLocationFromMessage(WORLD_MESSAGE message);

    public void setLocation(Location loc) {
        this.location = loc;
    }

    public abstract boolean getVisibilityFromMessage(WORLD_MESSAGE message);

    public void setVisibility(boolean isVisible) {
        this.isVisible = isVisible;
    }

    public Location getLocation() {
        return location;
    }

    public boolean isVisible() {
        return isVisible;
    }
    
    /**
     * Checks if info is up-to-date compared to current sim time
     *
     * @param currentSimTime
     * @return if is up-to-date
     */
    public final boolean isActual(long currentSimTime) {
        return (currentSimTime - lastUpdated) < getMaxUpdateDelay();
    }

    protected abstract int getMaxUpdateDelay();

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorldObjectState<?, ?> other = (WorldObjectState<?, ?>) obj;
        return (this.compareTo(other.id) == 0);
    }

    @Override
    public int compareTo(WorldObjectId o) {
        return (this.id.equals(o)) ? 0 : -1;
    }
    
}
