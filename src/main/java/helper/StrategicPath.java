/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package helper;

import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.NavPoint;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * One strategic path to enemy flag with one starting meetpoint
 * @author root
 */
public class StrategicPath {
    private MeetPoint mp;
    private List<NavPoint> path;
    private List<NavPoint> pathBack;

    public StrategicPath(MeetPoint mp, List<NavPoint> path) {
        this.mp = mp;
        this.path = path;
    }
    
    public List<NavPoint> getPathBack() {
        if(pathBack == null) {
            pathBack = new ArrayList<NavPoint>(path);
            Collections.reverse(pathBack);
            pathBack.add(mp.getNavPoint());
        }
        
        return pathBack;
    }

    public MeetPoint getMeetPoint() {
        return mp;
    }

    public List<NavPoint> getPath() {
        return path;
    }
    
}
