/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package helper;

import action.FlagCapture;
import ctfbot.CTFMaster;
import cz.cuni.amis.pogamut.base.communication.command.IAct;
import cz.cuni.amis.pogamut.base.utils.logging.LogCategory;
import cz.cuni.amis.pogamut.base3d.worldview.IVisionWorldView;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensomotoric.AdrenalineCombo;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensomotoric.AgentConfig;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensomotoric.Raycasting;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensomotoric.Weaponry;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensor.AgentInfo;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensor.AgentStats;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensor.CTF;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensor.Game;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensor.ItemDescriptors;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensor.Items;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensor.NavPoints;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensor.NavigationGraphBuilder;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensor.Players;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensor.Senses;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensor.WeaponPrefs;
import cz.cuni.amis.pogamut.ut2004.agent.module.sensor.visibility.Visibility;
import cz.cuni.amis.pogamut.ut2004.agent.navigation.IUT2004Navigation;
import cz.cuni.amis.pogamut.ut2004.agent.navigation.UT2004AStarPathPlanner;
import cz.cuni.amis.pogamut.ut2004.agent.navigation.UT2004MapTweaks;
import cz.cuni.amis.pogamut.ut2004.agent.navigation.astar.UT2004AStar;
import cz.cuni.amis.pogamut.ut2004.agent.navigation.floydwarshall.FloydWarshallMap;
import cz.cuni.amis.pogamut.ut2004.agent.navigation.navmesh.LevelGeometryModule;
import cz.cuni.amis.pogamut.ut2004.agent.navigation.navmesh.NavMeshModule;
import cz.cuni.amis.pogamut.ut2004.agent.navigation.navmesh.pathfollowing.NavMeshNavigation;
import cz.cuni.amis.pogamut.ut2004.bot.command.AdvancedLocomotion;
import cz.cuni.amis.pogamut.ut2004.bot.command.CompleteBotCommandsWrapper;
import cz.cuni.amis.pogamut.ut2004.bot.command.ImprovedShooting;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.Player;
import cz.cuni.amis.pogamut.ut2004.teamcomm.bot.UT2004TCClient;
import roles.RoleService;

/**
 *
 * @author root
 */
public abstract class AKnowledgeBase {

    protected final AgentInfo info;
    protected final Players players;
    protected final AgentStats stats;
    protected final WeaponPrefs weaponPrefs;
    protected final Items items;
    protected final CompleteBotCommandsWrapper body;
    protected final FloydWarshallMap fwMap;
    protected final NavMeshNavigation nmNav;
    protected final Weaponry weaponry;
    protected final UT2004MapTweaks mapTweaks;
    protected final Game game;
    protected final ItemDescriptors descriptors;
    protected final Senses senses;
    protected final AgentConfig config;
    protected final Raycasting raycasting;
    protected final ImprovedShooting shoot;
    protected final AdvancedLocomotion move;
    protected final CTF ctf;
    protected final AdrenalineCombo combo;
    protected final NavigationGraphBuilder navBuilder;
    protected final IVisionWorldView world;
    protected final IAct act;
    protected final Visibility visibility;
    protected final UT2004AStarPathPlanner ut2004PathPlanner;
    protected final NavPoints navPoints;
    protected final UT2004AStar aStar;
    protected final NavMeshModule navMeshModule;
    protected final LevelGeometryModule levelGeometryModule;
    protected final LogCategory log;
    protected final CTFMaster bot;
    protected final RoleService roleService;
    protected final UT2004TCClient com;
    protected final IUT2004Navigation botNav;

    public AKnowledgeBase(CTFMaster bot) {
        this.nmNav = bot.getNMNav();
        this.mapTweaks = bot.getMapTweaks();
        this.game = bot.getGame();
        this.info = bot.getInfo();
        this.players = bot.getPlayers();
        this.descriptors = bot.getDescriptors();
        this.items = bot.getItems();
        this.senses = bot.getSenses();
        this.weaponry = bot.getWeaponry();
        this.config = bot.getConfig();
        this.raycasting = bot.getRaycasting();
        this.body = bot.getBody();
        this.shoot = bot.getShoot();
        this.move = bot.getMove();
        this.ctf = bot.getCTF();
        this.combo = bot.getCombo();
        this.ut2004PathPlanner = bot.getUT2004AStarPathPlanner();
        this.navBuilder = bot.getNavBuilder();
        this.weaponPrefs = bot.getWeaponPrefs();
        this.world = bot.getWorld();
        this.act = bot.getAct();
        this.stats = bot.getStats();
        this.fwMap = bot.getFwMap();
        this.visibility = bot.getVisibility();
        this.navPoints = bot.getNavPoints();
        this.aStar = bot.getAStar();
        this.navMeshModule = bot.getNavMeshModule();
        this.levelGeometryModule = bot.getLevelGeometryModule();
        this.log = bot.getLog();
        this.roleService = bot.getRoleService();
        this.com = bot.getTCClient();
        this.botNav = bot.getNavigation();
        
        this.bot = bot;
    }
    
    public boolean isOurTeam(int team) {
        return info.getTeam().equals(team);
    }

    public boolean isEnemy(Player p) {
        return !isOurTeam(p.getTeam());
    }


}
