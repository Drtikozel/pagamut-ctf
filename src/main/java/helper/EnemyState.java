/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package helper;

import communication.TCEnemySeen;
import cz.cuni.amis.pogamut.base.communication.worldview.object.WorldObjectId;
import cz.cuni.amis.pogamut.base3d.worldview.object.Location;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.Player;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.PlayerMessage;

/**
 * Enemy state is 1:1 relation to Player and it's UnrealId, so it is compared by it
 * @author root
 */
public class EnemyState extends WorldObjectState<PlayerMessage, TCEnemySeen>{

    public EnemyState(WorldObjectId id) {
        super(id);
    }

//    public EnemyState(Player p, boolean isVisible) {
//        super(p.getId());
//        setLocation(p.getLocation());
//        setVisibility(isVisible);
//    }

    @Override
    public void setLocation(Location loc) {
        super.setLocation(loc); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Location getLocationFromMessage(PlayerMessage message) {
        return message.getLocation();
    }

    @Override
    public boolean getVisibilityFromMessage(PlayerMessage message) {
        return message.isVisible();
    }
    
    @Override
    protected int getMaxUpdateDelay() {
        return 2000;
    }

    @Override
    public String toString() {
        return "EnemyState{" + this.location +  '}';
    }
    
}
