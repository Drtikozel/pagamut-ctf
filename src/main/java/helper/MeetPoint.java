/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package helper;

import cz.cuni.amis.pogamut.base3d.worldview.object.ILocated;
import cz.cuni.amis.pogamut.base3d.worldview.object.Location;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.NavPoint;
import cz.cuni.amis.utils.Cooldown;
import cz.cuni.amis.utils.Heatup;
import java.util.Random;

/**
 *
 * @author root
 */
public class MeetPoint extends AInterestingPoint {
    
    private final NavPoint post;
    private Location location;
    
    
    // bude čekat 5 sekund
    Heatup heat = new Heatup(5000);

    public MeetPoint(NavPoint post) {
        this.post = post;
    }
    
    /**
     * Notify that bot is on point
     */
    @Override
    public void inPosition() {
        heat.heat();
        super.inPosition();
    }
    
    public boolean isTimeToGo() {
        return heat.isCool();
    }

    @Override
    public Location getLocation() {
        Random r = new Random(System.currentTimeMillis());
        if(location == null) {
            this.location = this.post.getLocation();
            this.location.addX((r.nextInt(200)-100));
            this.location.addY((r.nextInt(200)-100));
        }
        
        return this.location;
    }

    public NavPoint getNavPoint() {
        return post;
    }
    
    @Override
    protected int getTabooTime() {
        return 0;
    }
}
