/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package helper;

import cz.cuni.amis.pogamut.ut2004.communication.messages.UT2004ItemType;

/**
 *
 * @author root
 */
public class WantedWeapon {
    public final UT2004ItemType weapon;
    public final UT2004ItemType primaryAmmo;

    public WantedWeapon(UT2004ItemType weapon, UT2004ItemType primaryAmmo) {
        this.weapon = weapon;
        this.primaryAmmo = primaryAmmo;
    }
//
//    @Override
//    public int hashCode() {
//        int hash = 3;
//        hash = 23 * hash + (this.weapon != null ? this.weapon.hashCode() : 0);
//        hash = 23 * hash + (this.primaryAmmo != null ? this.primaryAmmo.hashCode() : 0);
//        return hash;
//    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof UT2004ItemType) {
            return (this.weapon.equals(obj) || this.primaryAmmo.equals(obj));
        } else {
            return false;
        }
    }
    
    
}
