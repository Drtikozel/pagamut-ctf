/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package helper;

import cz.cuni.amis.pogamut.ut2004.agent.module.sensor.AgentInfo;
import cz.cuni.amis.pogamut.ut2004.agent.navigation.floydwarshall.FloydWarshallMap;
import cz.cuni.amis.pogamut.ut2004.communication.messages.gbinfomessages.NavPoint;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author root
 */
public class EnemyMovementInfoService {

    EnemyState enemy;
    StrategicPath strategicPath;
    TreeMap<Double, PathInfo> enemysDistanceToPath = new TreeMap<Double, PathInfo>();
    private final FloydWarshallMap fwMap;
    private final AgentInfo info;

    public EnemyMovementInfoService(EnemyState enemy, FloydWarshallMap fwMap, AgentInfo info) {
        this.enemy = enemy;
        this.fwMap = fwMap;
        this.info = info;
    }

    public void countDistanceToPath(StrategicPath path) {
        double nearestPoint = Double.MAX_VALUE;
        int nearestPointElementNumber = 1;
        int pathElementNumber = 1;

        PathInfo pathInfo = new PathInfo(path);

        for (NavPoint pathElement : path.getPathBack()) {
            double distance = fwMap.getDistance(info.getNearestNavPoint(enemy.getLocation()), pathElement);
            pathInfo.totalDistance += distance;
            if (distance <= nearestPoint) {
                nearestPoint = distance;
                pathInfo.nearestElement = pathElementNumber;
            }
        }
    }
    
    public PathInfo getEnemyPathInfo() {
        if(!enemysDistanceToPath.isEmpty()) {
            return enemysDistanceToPath.firstEntry().getValue();
        } else {
            return null;
        }
    }
    
    public PathInfo getSecondNearestPathInfo() {
        if(!enemysDistanceToPath.isEmpty()) {
            Collection<PathInfo> infos = enemysDistanceToPath.values();
            Iterator<PathInfo> it = infos.iterator();
            
            // hack!! If there is only one path, return it
            PathInfo secondNearest = it.next();
            if(it.hasNext()) {
                secondNearest = it.next();
            }
            
            return secondNearest;
        } else {
            return null;
        }
    }
    
    public PathInfo getFurthestPathInfo() {
        if (!enemysDistanceToPath.isEmpty()) {
            return enemysDistanceToPath.lastEntry().getValue();
        } else {
            return null;
        }
    }

    public class PathInfo {

        public StrategicPath strategicPath;
        public double totalDistance;
        public int nearestElement;

        public PathInfo(StrategicPath strategicPath) {
            this.strategicPath = strategicPath;
            this.totalDistance = Double.MAX_VALUE;
            this.nearestElement = -1;
        }
    }

}
