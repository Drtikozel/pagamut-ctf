/*
 * Copyright (C) 2015 AMIS research group, Faculty of Mathematics and Physics, Charles University in Prague, Czech Republic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package helper;

import action.FlagCapture;
import cz.cuni.amis.utils.Heatup;

/**
 *
 * @author root
 */
public class NextFlagAction {
    private FlagCapture.State plannedState;
    private final Heatup heatup;
    private final int HEATUP_TIME = 2000;

    public NextFlagAction(FlagCapture.State plannedState, boolean needsHeatup) {
        this.plannedState = plannedState;
        this.heatup = new Heatup(HEATUP_TIME);
        this.heatup.heat();
    }
    
    public boolean isTimedOut() {
        return this.heatup.isCool();
    }

    public FlagCapture.State getPlannedAction() {
        return plannedState;
    }
    
}
